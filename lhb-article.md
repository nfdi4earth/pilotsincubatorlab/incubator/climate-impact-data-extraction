---
name: Web-based, user-driven climate impact data extraction
description: -<
  The ISIMIP Repository of the ISIMIP project holds the world's largest collection of global
  climate impact model data. However, both the format (NetCDF) and file sizes represent a major
  barrier for many users. We here propose to build an innovative web-based service that allows
  users to extract, process and download subsets of the data. User-defined extraction, chained
  processing and data interaction through scripts and interactive Jupyter notebooks will largely
  widen the user base. Users can initiate processing tasks in the cloud and download the resulting
  files in different formats. The code will be released as open source software and, as the
  application is not tied to ISIMIP or the ISIMIP conventions, can be adopted for similar archives
  of NetCDF files.

author:
  - family-names: Klar
    given-names: Jochen
    orcid: 'https://orcid.org/0000-0002-5883-4273'
  - family-names: Mengel
    given-names: Matthias
    orcid: 'https://orcid.org/0000-0001-6724-9685'

language: ENG

collection:
  - N4E_Incubators.md

subtype: article

subject:
  - dfgfo:313%0A
  - unesco:concept198
  - unesco:concept4559
  - unesco:mt2.35

keywords:
  - NetCDF
  - Web service
  - ISIMIP

target_role:
  - data curator
  - data provider
  - service provider

identifier:

version: 1.0
---

# Web-based, user-driven climate impact data extraction

## Abstract

The Inter-sectoral Impact Model Intercomparison Project (ISIMIP) brings together worldwide
climate impact modeling communities through a consistent modeling setup across sectors, the
provision of high-quality forcing data and the curation of the output data in the ISIMIP
Repository. As climate impact data provides the link between climate change and society, there
is a high demand for reuse of the data across many fields. Data is typically stored on a grid
covering the global domain with typical file sizes of hundreds of GB and uses the NetCDF
format, which is very common in climate science, but lacks distribution in other communities.
The size and the format are strong barriers for many users. In the past, the ISIMIP data team
needed to manually prepare special conversions of datasets for download, which was a
cumbersome and time consuming task. Many users are only interested in data on particular
countries, regions or individual places with much smaller potential sizes than the source files.

To overcome these barriers, we initiated the development of a web-based service as part of the
BMBF funded ISI-Access project: the ISIMIP Files API enables the user to interact with the
storage cloud via the internet from anywhere in the world, run a predefined set of regularly
requested processing tasks and download the results (e.g. as .CSV). As of now, the API is able
to perform cut-outs of regions, masking of areas and countries and selecting time series for
points, countries or areas. The service is already in use and enjoys great popularity.

More details about the project can be found in the full [proposal text](proposal.pdf).

The project was presented at the
[3rd NFDI4Earth Plenary 2024](https://www.nfdi4earth.de/nfdi4earth-plenary-2024),
you can find the poster [here](poster.pdf).

For a more technical description on how the API works, please have a look at it's
[README on GitHub](https://github.com/ISI-MIP/isimip-files-api/).

## Outcomes

The NFDI4Earth incubator allowed us to improve the API in three ways:

- User-defined shapes can be uploaded, e.g. ocean regions or river catchments,
  to perform cutout and extraction. We provide compatibility with GeoJSON and
  shapefile formats. The extracted shapes can be further processed, e.g. their
  data averaged or summed and then downloaded as time series.
- Inspired by the [CDO](https://code.mpimet.mpg.de/projects/cdo/) feature of chained
  operators, we implement a similar feature in the ISIMIP Files API. A use case for this
  is the cut-out of a rectangular bounding box from high-resolution data and the subsequent
  masking of a region from an uploaded shape. The operators are taken from a
  predefined list, which ensures flexibility but also security. The software is structured
  in a way that new operations can be added with ease.
- The API can be used in a user-friendly way using scripts and Jupyter notebooks. This will
  enable the users to access all functionality and all newly developed functions in a
  programmatic way following current practice in data science. It will also allow the
  users to run larger batches of tasks in a reproducible way.

The code is released as open source software under the MIT license on GitHub:

<https://github.com/ISI-MIP/isimip-files-api>

As the application is not tied to ISIMIP conventions, can be adopted for similar archives
of NetCDF files. The setup is carefully documented and a docker setup is available.
Releases on PyPI will follow.

All features developed as part of this proposal will be integrated into the ISIMIP Repository
and will be usable by the community when downloading data from the Repository. The new version
of the API is usable at:

<https://files.isimip.org/api/v2>

We provide two example notebooks in this repository on how to use the API:

1. [Select a time series for a region](notebooks/1-select-time-series.ipynb)
2. [Mask country from high-resolution data](notebooks/2-mask-region-high-resolution.ipynb)

We also provide [R](https://www.r-project.org/) versions of the notebooks in the [R](R) directory.

The new features will also be into the Repository's web portal https://data.isimip.org later
this year.

## Resources

### Source code and documentation

* Source code on GitHub <https://github.com/ISI-MIP/isimip-files-api>
* Source code on Zenodo <https://doi.org/10.5281/zenodo.10371806>
* NFDI4Earth incubator repository <https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/climate-impact-data-extraction>

### More about ISIMIP

* ISIMIP Project <https://www.isimip.org>
* ISIMIP Repository <https://data.isimip.org>
* File API of the ISIMIP Repository <https://files.isimip.org/api/v2>
